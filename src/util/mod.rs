use anyhow::{anyhow, Context, Error, Result};
use egs_api::api::types::account::{AccountData, AccountInfo, UserData};
use egs_api::api::types::asset_info::AssetInfo;
use egs_api::api::types::epic_asset::EpicAsset;
use serde_json::Value;
use std::env;
use std::fs::create_dir_all;
use std::fs::{File, OpenOptions};
use std::io::{BufReader, Read, Write};

pub const HIDDEN_DIR: &str = ".ue-marketplace-cli";
pub const ASSET_CACHE: &str = "asset";
pub const USER_DATA: &str = "cache";

pub fn get_cache_dir() -> String {
    format!(
        "{}/{}",
        env::var("HOME").unwrap_or("./".to_string()), HIDDEN_DIR
    )
}

/// Stores user login data in a cache file. Location of the cache file is
/// configued by default but the fields are public to allow for any
/// future customization.
pub struct DataCache {
    pub cache_dir: String,
    pub file_name: String,
}
impl Default for DataCache {
    fn default() -> Self {
        DataCache {
            cache_dir: get_cache_dir(),
            file_name: self::USER_DATA.to_string(),
        }
    }
}
impl DataCache {
    // pub fn new(cache_dir: String, file_name: String) -> DataCache {
    //     DataCache {
    //         cache_dir,
    //         file_name
    //     }
    // }
    pub fn new_file(file_name: String) -> DataCache {
        DataCache {
            cache_dir: get_cache_dir(),
            file_name
        }
    }

    /// Returns true if cache exists, false otherwise.
    pub fn exists(&self) -> bool {
        if let Ok(_file) = std::fs::File::open(format!("{}/{}", self.cache_dir, self.file_name)) {
            true
        } else {
            false
        }
    }

    /* TODO: Change to use generic types */
    pub fn load_user_data(&mut self) -> Result<UserData> {
        let cache_file_path = format!("{}/{}", self.cache_dir, self.file_name);

        let mut cache = BufReader::new(File::open(&cache_file_path)?);

        // read from cache
        let mut buf = String::new();
        cache.read_to_string(&mut buf)?;

        if let Ok(data) = serde_json::from_str(&buf) {
            Ok(data)
        } else {
            eprint!("Could not deserialize JSON.\n");
            Err(anyhow!("Could not deserilze JSON"))
        }
    }

    pub fn save_user_data(&self, data: &UserData) {
        // Create storage cache directory if it has not been created yet
        create_dir_all(&self.cache_dir).expect("Could not create cache file and directory.");

        let cache_file_path = format!("{}/{}", self.cache_dir, self.file_name);
        let mut cache = OpenOptions::new()
            .read(true)
            .write(true)
            .create(true)
            .open(&cache_file_path)
            .expect(format!("Could not find cache file: {}", &cache_file_path).as_str());

        //write to cache
        cache
            .write_all(
                serde_json::to_string(data)
                    .expect("Could not write User Data to cache file.")
                    .as_bytes(),
            )
            .expect("Could not write User Data to cache file.");
    }

    pub fn load_asset_list(&mut self) -> Result<Vec<Option<AssetInfo>>> {
        let cache_file_path = format!("{}/{}", self.cache_dir, self.file_name);

        let mut cache = BufReader::new(File::open(&cache_file_path)?);

        // read from cache
        let mut buf = String::new();
        cache.read_to_string(&mut buf)?;

        if let Ok(data) = serde_json::from_str(&buf) {
            Ok(data)
        } else {
            eprint!("Could not deserialize JSON.\n");
            Err(anyhow!("Could not deserilze JSON"))
        }
    }

    pub fn save_asset_list(&self, data: &Vec<Option<AssetInfo>>) {
        // Create storage cache directory if it has not been created yet
        create_dir_all(&self.cache_dir).expect("Could not create cache file and directory.");

        let cache_file_path = format!("{}/{}", self.cache_dir, self.file_name);
        let mut cache = OpenOptions::new()
            .read(true)
            .write(true)
            .create(true)
            .open(&cache_file_path)
            .expect(format!("Could not find cache file: {}", &cache_file_path).as_str());

        //write to cache
        cache
            .write_all(
                serde_json::to_string(data)
                    .expect("Could not write User Data to cache file.")
                    .as_bytes(),
            )
            .expect("Could not write User Data to cache file.");
    }
    /* ******************************** */

    pub fn clear_cache_file(&self) {
        let cache_file_path = format!("{}/{}", self.cache_dir, self.file_name);
        std::fs::remove_file(&cache_file_path);
    }
}
