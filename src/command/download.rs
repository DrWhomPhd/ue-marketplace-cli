use std::{
    collections::VecDeque, ffi::OsStr, fs::File, io::{Read, Seek, Write}, ops::Deref, os::unix::fs::{FileExt, MetadataExt}, path::Path
};

use sha1_checked::Sha1;
use clap::ArgMatches;
use egs_api::EpicGames;
use reqwest::blocking::Client;

use crate::util::{self, DataCache};

pub fn download_item(download_cmd: &ArgMatches, api_entry: &mut EpicGames) {
    let id = download_cmd
        .get_one::<String>("ID")
        .expect("Required argument ID missing.");

    let appid = download_cmd
        .get_one::<String>("APPID")
        .expect("Required argument APPID mising.");

    let app_manifest = api_entry.asset_manifest(
        None,
        None,
        Some("ue".to_string()),
        Some(id.to_string()),
        Some(appid.to_string()),
    );
    if let Some(app_manifest) = app_manifest {
        let dl_folder_name = format!(
            "{}/{}_{}",
            util::get_cache_dir(),
            app_manifest.app.as_ref().unwrap_or(&"UNKNOWN".to_string()),
            app_manifest
                .item_id
                .as_ref()
                .unwrap_or(&"UNKNOWN".to_string())
        );

        std::fs::create_dir_all(&dl_folder_name).expect("Could not create download directories.");

        let dl_manifest = api_entry.asset_download_manifests(app_manifest);

        // Assume the first manifest is the best manifest
        let manifest = &dl_manifest[0];
        let file_manifest_list = &dl_manifest[0].file_manifest_list;

        // Setup our http client
        let dl_client = Client::new();

        for m in manifest.files().values() {
            for chunk in &m.file_chunk_parts {
                let domain = chunk.link.as_ref().unwrap().as_str();
                let chunk_file_name = format!("{}/{}.chunk", dl_folder_name, chunk.guid);
                // The try_exists function is currently experimental, instead we will try to get
                // the metadata for a file assuming we will get an error if the file doesn't exist
                if let std::io::Result::Err(x) = std::fs::metadata(&chunk_file_name) {
                    println!("Downloading: {}", &domain);
                    if let Ok(dl_chunk) = dl_client.get(domain).send() {
                        let mut chunk_file = File::options()
                            .write(true)
                            .create(true)
                            .open(&chunk_file_name)
                            .expect("Error opening chunk.");
                        chunk_file
                            .write_all(&dl_chunk.bytes().unwrap())
                            .expect("Could not write out chunk.");
                    }
                }
            }
        }

        // Can't check chunk SHA sums as the GUID -> SHA Hashmap doesn't match any known sha result for the chunk.
        for file in file_manifest_list {
            // Create file with filename
            dbg!(file);
            let asset = format!("{}/{}", &dl_folder_name, &file.filename);
            let asset_file_path = Path::new(&asset);
            std::fs::create_dir_all(&asset_file_path.parent().unwrap()).expect("Could not create download directories.");
            let mut outfile = File::options()
                .write(true)
                .create(true)
                .read(true)
                .open(&asset_file_path)
                .expect(format!("Coud not open: {}", asset_file_path.to_string_lossy()).as_str());

            // Loop through each chunk, open guid, and write the part to outfile.
            for chunk in &file.file_chunk_parts {
                let chunk_name = format!("{}/{}.chunk", dl_folder_name, chunk.guid);
                let mut chunk_file = File::open(&chunk_name)
                    .expect(format!("Couldn't open: {}", &chunk_name).as_str());
                
                // Create an intermediate buffer
                let mut buffer = Vec::<u8>::new();
                buffer.resize(chunk_file.metadata().unwrap().size() as usize, 0u8);           
                chunk_file.read_exact(&mut buffer)
                    .expect("Unable to read from chunk");
                                
                let chunk_file = egs_api::api::types::chunk::Chunk::from_vec(buffer).expect("Could not parse chunk.");

                let write_sz = outfile.write(&chunk_file.data[chunk.offset as usize..(chunk.offset+chunk.size) as usize])
                    .expect("Unable to write to file");
                assert_eq!(chunk.size as usize, write_sz);
            }

            // Check the outfile hash using file.file_hash
            let mut buf: Vec<u8> = Vec::new();
            buf.resize(outfile.metadata().unwrap().size() as usize, 0u8);

            outfile.read_at(&mut buf, 0).expect("Error reading Asset file");
            if (!check_sha1_hash(&buf, &file.file_hash)) { 
                panic!("SHA1 Hash Failed: {}", &file.filename);
            }
        }
    }
}

fn check_sha1_hash(buffer: &[u8], sha_hash: &String) -> bool {
    let result = Sha1::try_digest(buffer);
    if result.has_collision() { panic!("Downloaded file has SHA1 Hash Collision.")}

    dbg!(&result);
    hexstr_eq_hex(result.hash().as_ref(), sha_hash) 
}

/// Compares a string representation of a hex string to the
/// byte representation of a hex string. Useful when
/// attempting to compare a hash to the string representation
/// of the hash
fn hexstr_eq_hex(buffer: &[u8], sha_hash: &String) -> bool {
    let c: Vec<char> = sha_hash.chars().collect();
    for i in (0..c.len()).step_by(2) {
        let res = c[i].to_digit(16).unwrap() * 16
            + c[i+1].to_digit(16).unwrap();
        if buffer[i/2] as u32 != res {return false}
    }
    true
}

#[cfg(test)]
mod tests {

    use hex_literal::hex;

    #[test]
    fn test_hexstr_eq_hex() {
        let hex = hex!("AABBCCDDEEFF00112233445566778899");
        let hex_str = String::from("AABBCCDDEEFF00112233445566778899");

        assert!(super::hexstr_eq_hex(&hex, &hex_str));
    }

    #[test]
    fn test_check_sha1_hash() {
        let hex_str = String::from("79d8cb5fcd6620093abf3dafd3097401b6fb0f6c");
        assert!(super::check_sha1_hash(b"AABBCCDDEEFF00112233445566778899", &hex_str));
    }
}