use egs_api::{
    api::types::{asset_info::AssetInfo, epic_asset::EpicAsset},
    EpicGames,
};

use crate::util;

pub fn update_assets(api_entry: &mut EpicGames) {
    // Only list 'ue' assets because there are assets returned that aren't for engine dev.
    let assets: Vec<EpicAsset> = api_entry
        .list_assets(None, None)
        .into_iter()
        .filter(|x| x.namespace == "ue")
        .collect();

    let asset_info_list: Vec<Option<AssetInfo>> = assets
        .into_iter()
        .map(|x| {
            if let Some(info) = api_entry.asset_info(x) {
                Some(info)
            } else {
                None
            }
        })
        .collect();

    println!("Updating cache...");
    let asset_cache = crate::util::DataCache::new_file(util::ASSET_CACHE.to_string());
    asset_cache.save_asset_list(&asset_info_list);
}
