use std::collections::HashMap;
use std::ops::Deref;

use clap::ArgMatches;
use egs_api::api::types::asset_info::AssetInfo;
use egs_api::EpicGames;
use egs_api::api::types::epic_asset::EpicAsset;

use crate::util;

#[derive(PartialEq, Copy, Clone)]
pub enum CategoryFilter {
    ENGINES = 0b100,
    ASSETS = 0b010,
    PLUGINS = 0b001,
    SHOWALL = 0b111,
    // GAMES [to add]
    // PROJECTS [to add]
}

pub fn print_assets(list_cmd: &ArgMatches, api_entry: &mut EpicGames) {
    let mut filters = CategoryFilter::SHOWALL as u8;

    if list_cmd.get_flag("engines") {
        filters = if filters == CategoryFilter::SHOWALL as u8 {
            CategoryFilter::ENGINES as u8
        } else {
            filters | CategoryFilter::ENGINES as u8
        }
    };
    if list_cmd.get_flag("assets") {
        filters = if filters == CategoryFilter::SHOWALL as u8 {
            CategoryFilter::ASSETS as u8
        } else {
            filters | CategoryFilter::ASSETS as u8
        }
    };
    if list_cmd.get_flag("plugins") {
        filters = if filters == CategoryFilter::SHOWALL as u8 {
            CategoryFilter::PLUGINS as u8
        } else {
            filters as u8 | CategoryFilter::PLUGINS as u8 
        }
    };

    let mut asset_cache = crate::util::DataCache::new_file(util::ASSET_CACHE.to_string());

    let asset_list = if asset_cache.exists() {
        asset_cache.load_asset_list().expect("Failed to load asset cache though file exists.")
    } else {
        // Only list 'ue' assets because there are assets returned that aren't for engine dev.
        let assets: Vec<EpicAsset> = api_entry
            .list_assets(None, None)
            .into_iter()
            .filter(|x| x.namespace == "ue")
            .collect();

        assets
            .into_iter()
            .map(|x| {
                if let Some(info) = api_entry.asset_info(x) {
                    Some(info)
                } else {
                    None
                }
            })
            .collect()
    };

    // Replace asset cache with the latest asset list
    asset_cache.save_asset_list(&asset_list);

    let mut release_id_count: HashMap<String, u8> = HashMap::new();

    print_header();
    for asset in asset_list {
        if let Some(info) = asset {
            if let Some(categories) = &info.categories {
                if filters & CategoryFilter::ASSETS as u8 == CategoryFilter::ASSETS as u8 {
                    if categories.iter().any(|x| x.path == "assets") {
                        print_item(info, &mut release_id_count);
                        continue;
                    }
                }
                if filters & CategoryFilter::ENGINES as u8 == CategoryFilter::ENGINES as u8 {
                    if categories.iter().any(|x| x.path == "engines") {
                        print_item(info, &mut release_id_count);
                        continue;
                    }
                }
                if filters & CategoryFilter::PLUGINS as u8 == CategoryFilter::PLUGINS as u8 {
                    if categories.iter().any(|x| x.path == "plugins") {
                        print_item(info, &mut release_id_count);
                        continue;
                    }
                }
            }
        }
    }
}

fn print_header() {
    println!("ID\t\t\t\t\tName");
}
fn print_item(item: AssetInfo, id_count: &mut HashMap<String, u8>) {
    let title = item.title.unwrap_or("No Title Available".to_string());
    let appid = 
        if let Some(release) = item.release_info {
            // id_count maps asset IDs to the number of time we've seen them. This is necessary
            // to get the right app_id from the release list as the same ID may have multiple
            // releases.
            let app_id = if id_count.contains_key(&item.id) {
                let count = id_count.get_mut(&item.id).expect("Couldn't get value for key that exists");
                *count = count.clone() + 1;
                &release[count.clone() as usize].app_id

            } else {
                let count: u8 = 0;
                id_count.insert(item.id.clone(), count);
                &release[count as usize].app_id
            };
            app_id.clone().unwrap_or(String::from("Unknown"))
        } else {
            "Unknown".to_string()
        };
    println!("{}\t{}\t{}", &item.id, appid, title);
}