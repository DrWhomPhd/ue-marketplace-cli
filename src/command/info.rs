use clap::{Arg, ArgMatches};
use egs_api::EpicGames;

use crate::util;

pub fn print_info(info_cmd: &ArgMatches, api_entry: &mut EpicGames) {
    let id = info_cmd
        .get_one::<String>("ID")
        .expect("Required argument ID missing.");

    let appid = info_cmd
        .get_one::<String>("APPID")
        .expect("Required argument APPID mising.");

    let mut asset_cache = util::DataCache::new_file(util::ASSET_CACHE.to_string());
    if let Ok(asset_cache) = asset_cache.load_asset_list() {
        let to_print = asset_cache.into_iter().find(|asset| {
            if let Some(asset) = asset {
                let asset_appid = if let Some(release) = &asset.release_info {
                    let app_id = &release[0].app_id;
                    app_id.clone().unwrap_or(String::from("Unknown"))
                } else {
                    "Unknown".to_string()
                };

                (&asset.id == id) && (&asset_appid == appid)
            } else {
                false
            }
        });

        // Currently temporary but it provides a quality
        // formatted print.
        dbg!(to_print);
    } else {
        println!("Could not load asset cache. Please run with 'upgrade'.")
    }
}
