use clap::{arg, command, ArgAction, Command};
use egs_api::EpicGames;

use ue_marketplace_cli::util::DataCache;
use ue_marketplace_cli::command::*;

fn main() {
    let mut auth_cache = DataCache::default();

    let matches = command!()
        .arg(arg!(-l --"login" <"AUTHCODE"> "Login to the UE Marketplace. Requires Authorization code to use for logging in."))
        .arg(arg!(--logout "Logout of the UE Marketplace").action(ArgAction::SetTrue))
        .subcommand(
            Command::new("list")
                .about("Lists various types of information available to the user from the Unreal Engine marketplace.")
                .arg(arg!(-e --engines "List engines available for install"))
                .arg(arg!(-a --assets "List assets available for install"))
                .arg(arg!(-p --plugins "List plugins needed for install."))
        )
        .subcommand(
            Command::new("info")
                .about("Provide detailed information regarding a UE Marketplace item.")
                .arg(arg!(<ID>))
                .arg(arg!(<APPID>))
        )
        .subcommand(
            Command::new("update")
                .about("Update the asset and information cache from the market. Requires login.")   
        )
        .subcommand(
            Command::new("download")
                .about("Download asset specified")
                .arg(arg!(<ID>))
                .arg(arg!(<APPID>))
        )
        .subcommand(
            Command::new("config")
                .about("Get and set configuration options for this program.")
        )
        .subcommand(
            Command::new("search")
                .about("Search for a specific package")
        )
        .get_matches();

    // API STATE
    let mut api_entry = EpicGames::new();

    // Load prior saved User Data
    if let Ok(data) = auth_cache.load_user_data() {
        api_entry.set_user_details(data);
    } else {
        println!("Failure loading user cache information\n");
    };

    // Will attempt to reuse current session before login
    if !api_entry.login() {
        {
            let this = &auth_cache;
            this.clear_cache_file();
            // let cache_file_path = format!("{}/{}", this.cache_location, this.login_cache_name);
            // std::fs::remove_file(&cache_file_path);
        };
        eprint!("Error refreshing session. Please login again by visiting: https://www.epicgames.com/id/login?redirectUrl=https%3A%2F%2Fwww.epicgames.com%2Fid%2Fapi%2Fredirect%3FclientId%3D34a02cf8f4414e29b15921876da36f9a%26responseType%3Dcode.\n");
    };

    // Login -- must be done before we do any else
    if let Some(auth_code) = matches.get_one::<String>("login") {
        println!("Logging in...");
        if api_entry.auth_code(None, Some(String::from(auth_code))) {
            println!("Logged in!");
            // Save user Data in Program Cash
            DataCache::default().save_user_data(&api_entry.user_details());
        } else {
            eprintln!("Error signing in. Most likely bad or stale authentication token. Please visit https://www.epicgames.com/id/login?redirectUrl=https%3A%2F%2Fwww.epicgames.com%2Fid%2Fapi%2Fredirect%3FclientId%3D34a02cf8f4414e29b15921876da36f9a%26responseType%3Dcode")
        }
    }
    // Eventually should split CLI operations between those that must be done logged in and those that work on cached data
    // right now everything below assumes the user is logged in.

    //
    if let Some(list_cmd) = matches.subcommand_matches("list") {
        list::print_assets(&list_cmd, &mut api_entry)
    }

    if let Some(info_cmd) = matches.subcommand_matches("info") {
        info::print_info(&info_cmd, &mut api_entry)
    }
    
    if let Some(download_cmd) = matches.subcommand_matches("download") {
        download::download_item(&download_cmd, &mut api_entry)
    }

    if let Some(_update_cmd) = matches.subcommand_matches("update") {
        println!("Updating asset list");
        update::update_assets(&mut api_entry);
    }
}
